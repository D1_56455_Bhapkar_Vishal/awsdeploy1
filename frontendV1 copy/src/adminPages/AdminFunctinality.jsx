import React from 'react'
import Card from '../components/Card'

const AdminFunctinality = () => {

    const admin = {
        link: "/addAdmin",
        msg: "For Register New Admin"
    }

    const addBus = {
        link: "/addBus",
        msg: "for Add New Bus"
    }

    const searchingBus = {
        link: "/searchingBus",
        msg: "Search Bus"
    }

    const forgetPassword = {
        link: "/forgetPassword",
        msg: "Change Password"
    }
    const busDetails = {
        link: "/busDetails",
        msg: "View All Buses"
    }
    const busDetails1 = {
        link: "/busDetails",
        msg: "Edit Bus"
    }
    const busDetails2 = {
        link: "/busDetails",
        msg: "Delete Bus"
    }
    const busDetails3 = {
        link: "/busDetails",
        msg: "Schedule Bus"
    }

    const listOfPassenger = {
        link: "/passList",
        msg: "List Of Passenger",
    };

    const listOfTickets = {
        link: "/cancelTicket",
        msg: "List Of Booked Ticket",
    };

    return (
        <div style={{ marginTop: "20px" }} className="container">
            <h1 style={{ color: "grey", textAlign: "center", fontFamily: "cursive", marginBottom: "20px" }}>Welcome {sessionStorage.firstName}</h1>
            <div className="row">
                <div className="col"><Card bhushan={admin} /></div>
                <div className="col"><Card bhushan={addBus} /></div>
                <div className="col"><Card bhushan={busDetails} /></div>
            </div>
            <div className="row">
                <div className="col"><Card bhushan={searchingBus} /></div>
                <div className="col"><Card bhushan={forgetPassword} /></div>
                <div className="col"><Card bhushan={busDetails3} /></div>
            </div>
            <div className="row">
                <div className="col"><Card bhushan={busDetails1} /></div>
                <div className="col"><Card bhushan={busDetails2} /></div>
                <div className="col"><Card bhushan={listOfPassenger} /></div>
            </div>
            <div className="row">
                <div className="col"></div>
                <div className="col"><Card bhushan={listOfTickets} /></div>
                <div className="col"></div>
            </div>

        </div>
    )
}

export default AdminFunctinality