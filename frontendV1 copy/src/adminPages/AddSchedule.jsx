import axios from 'axios'
import React, { useState } from 'react'
import { useLocation } from 'react-router'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import { URL } from '../config'

const AddSchedule = () => {
    const { state } = useLocation()

    const busData = state.busData
    console.log(busData)
    const busId = busData.id;
    console.log(busId)

    const [dateOfTravelling, setDateOfTravelling] = useState('');


    const navigate = useNavigate();
    console.log(dateOfTravelling);

    const addSchedule = () => {
        if (busId === 0)
            toast.warning("Bus not found..!!")
        else if (dateOfTravelling.length === 0)
            toast.warning("Please select the date..!!")
        else {
            const body = {
                busId,
                dateOfTravelling
            }
            const url = `${URL}/busSchedule/addSchedule`
            axios.post(url, body).then((response) => {
                const result = response.data
                if (result['status'] === 'success') {
                    toast.success(`${busData.BusName} Bus Schedule added Successfully`)
                    navigate("/busDetails")
                    console.log(result.data)
                }
                else
                    toast.error(result['error'])
            })
            console.log(body)
        }


    }

    return (
        <div style={{ marginTop: "20px" }} className='container'>
            <h1>Bus Details</h1>
            <table style={{ border: "solid" }} className="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">Bus Id</th>
                        <th scope="col">Bus Name</th>
                        <th scope="col">Start City</th>
                        <th scope="col">Destination City</th>
                        <th scope="col">Departure Time</th>
                        <th scope="col">Reach Time</th>
                        <th scope="col">AC Sleeper Fair</th>
                        <th scope="col">AC Seating Fair</th>
                        <th scope="col">non-AC Sleeper Fair</th>
                        <th scope="col">non-AC Seating Fair</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{busData.id}</td>
                        <td>{busData.busName}</td>
                        <td>{busData.startCity}</td>
                        <td>{busData.destCity}</td>
                        <td>{busData.departureTime}</td>
                        <td>{busData.reachTime}</td>
                        <td>{busData.acSleeperSeatPrice}</td>
                        <td>{busData.acSeatingSeatPrice}</td>
                        <td>{busData.nonAcSleeperSeatPrice}</td>
                        <td>{busData.nonAcSeatingSeatPrice}</td>
                    </tr>
                </tbody>
            </table>
            <div className="row">
                <div className="row">
                    <div className="label-control ">Add Date :</div>

                    <input id='2' onChange={(e) => {
                        setDateOfTravelling(e.target.value)
                    }} type="date" className="form-control" >

                    </input>
                </div>
                <div className="col"></div>
                <div className="col"></div>
                <div className="col">
                    <button onClick={addSchedule}
                        className="btn-primary"
                        style={{ borderRadius: "7px", marginTop: "50px", width: "100%", height: "40px" }}>
                        Add Schedule
                    </button>
                </div>
                <div className="col"></div>
            </div>
        </div>
    )
}

export default AddSchedule