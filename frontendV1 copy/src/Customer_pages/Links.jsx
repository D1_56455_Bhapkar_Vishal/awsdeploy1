import React from 'react'
import { Link } from 'react-router-dom';

const Links = () => {
    return (
        <div>
            <Link to="/signin"><h1>Sign In</h1></Link>
            <Link to="/signup"><h1>Sign Up</h1></Link>
            <Link to="/addBus"><h1>addBus</h1></Link>
            <Link to="/busDetails"><h1>busDetails</h1></Link>
            <Link to="/forgetPassword"><h1>forgetPassword</h1></Link>
            <Link to="/searchingBus"><h1>searchingBus</h1></Link>
        </div>
    )
}

export default Links