import { useState, useEffect } from "react";
import axios from "axios";
import { URL } from "../config";
import Bus from "../components/Bus";
import { useNavigate } from "react-router";
import Book from "../components/Book";


const ListOfBuses = () => {
    const navigate = useNavigate();
    const [buses, setBuses] = useState([]);
    // console.log(sessionStorage.id);
    // console.log(sessionStorage.firstName);

    useEffect(() => {
        const url = `${URL}/buses/`;
        console.log("Inside get all buses");
        axios.get(url).then((response) => {
            const result = response.data;
            setBuses(result.data);

        });
    }, []);

    const styles = {
        table: {
            marginTop: "10px",
            border: "solid",
        },
    };

    return (
        <div className="container">
            <div className="container">
                <div className="row">
                    <div className="col">
                    </div>
                </div>
            </div>
            <h2 style={{ textAlign: "center" }}>Bus Details</h2>
            {buses.map((t) => {
                return <Book key={t.id} sam={t} />;
            })}
        </div>
    );
};

export default ListOfBuses;
