import React from "react";
import Card from "../components/Card";

const UserFunctionality = () => {
  const searchingBus = {
    link: "/searchingBus",
    msg: "Book Bus",
  };

  const forgetPassword = {
    link: "/forgetPassword",
    msg: "Change Password",
  };

  const listOfBuses = {
    link: "/list",
    msg: "List Of Buses",
  };

  const listOfPassenger = {
    link: "/passList",
    msg: "List Of Passenger",
  };

  const listOfTickets = {
    link: "/cancelTicket",
    msg: "List Of Booked Tickets",
  };

  return (

    <div style={{ marginTop: "20px", marginBottom: "150px" }} className="container">
      <h1
        style={{
          color: "grey",
          textAlign: "center",
          fontFamily: "cursive",
          marginBottom: "20px",
        }}
      >
        Welcome {sessionStorage.firstName}
      </h1>
      <div className="row">
        <div className="col">
          <Card bhushan={searchingBus} />
        </div>
        <div className="col">
          <Card bhushan={forgetPassword} />
        </div>
        <div className="col">
          <Card bhushan={listOfBuses} />
        </div>
      </div>
      <div className="row">
        <div className="col">
          <Card bhushan={listOfPassenger} />
        </div>
        <div className="col">
          <Card bhushan={listOfTickets} />
        </div>
      </div>
    </div>

  );
};

export default UserFunctionality;
