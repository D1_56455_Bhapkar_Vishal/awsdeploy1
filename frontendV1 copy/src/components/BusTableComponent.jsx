import React from 'react'

const BusTableComponent = (props) => {

    const busData = props.sam

    return (

        <div style={{ marginTop: "20px" }} className='container'>
            <h1>Bus Details</h1>
            <table style={{ border: "solid" }} className="table table-hover">
                <thead className="table-dark">
                    <tr>
                        <th scope="col">Bus Id</th>
                        <th scope="col">Bus Name</th>
                        <th scope="col">Start City</th>
                        <th scope="col">Destination City</th>
                        <th scope="col">Departure Time</th>
                        <th scope="col">Reach Time</th>
                        <th scope="col">AC Sleeper Fare</th>
                        <th scope="col">AC Seating Fare</th>
                        <th scope="col">non-AC Sleeper Fare</th>
                        <th scope="col">non-AC Seating Fare</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{busData.id}</td>
                        <td>{busData.busName}</td>
                        <td>{busData.startCity}</td>
                        <td>{busData.destCity}</td>
                        <td>{busData.departureTime}</td>
                        <td>{busData.reachTime}</td>
                        <td>{busData.acSleeperSeatPrice}</td>
                        <td>{busData.acSeatingSeatPrice}</td>
                        <td>{busData.nonAcSleeperSeatPrice}</td>
                        <td>{busData.nonAcSeatingSeatPrice}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}

export default BusTableComponent