import { BrowserRouter, Routes, Route, Link } from 'react-router-dom'
import Footer from './components/Footer';
import Header from './components/Header';
import Home from './pages/Home';
import AddBranch from './pages/AddBranch';
import EditBranch from './pages/EditBranch';
import AddComapany from './pages/AddCompany';
import EditCompany from './pages/EditCompany';

// import { ToastContainer } from 'react-toastify';
import './App.css';

function App() {
  return (
    <div className='container-fluid'>
      <Header />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/addCompany" element={<AddComapany />} />
          <Route path="/editCompany" element={<EditCompany />} />
          <Route path="/addBranch" element={<AddBranch />} /> 
          <Route path="/editBranch" element={<EditBranch />} />    

        </Routes>
      </BrowserRouter>
      <Footer />
      {/* <ToastContainer theme="colored" /> */}
    </div> 
  );
}

export default App;
